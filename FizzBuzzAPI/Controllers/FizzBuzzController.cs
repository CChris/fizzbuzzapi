﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FizzBuzzAndRooms.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FizzBuzzAndRooms.Controllers
{
    [Route("api/fizzbuzz")]
    [ApiController]
    public class FizzBuzzController : Controller
    {
        private readonly FizzBuzzContext _context;
        private readonly ILogger _logger;

        public FizzBuzzController(FizzBuzzContext context, ILogger<FizzBuzzController> logger)
        {
            _context = context;
            _logger = logger;
            if (_context.FizzBuzzItems.Count() != 0) return;

            _context.FizzBuzzItems.Add(new FizzBuzzItem() { Number = 6, Output = $"FizzBuzz"});
            _context.SaveChanges();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<FizzBuzzItem>>> GetFizzBuzzItems()
        {
            _logger.LogInformation($"Getting list of FizzBuzz items.");
            return await _context.FizzBuzzItems.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<FizzBuzzItem>> GetFizzBuzzItem(long id)
        {
            var fizzBuzzItem = await _context.FizzBuzzItems.FindAsync(id);

            _logger.LogInformation($"Getting FizzBuzz item {id}");

            if (fizzBuzzItem != null) return fizzBuzzItem;

            _logger.LogWarning($"[GET] Couldn't find FizzBuzz item at {id}");
            return NotFound();

        }

        [HttpPost]
        public async Task<ActionResult<FizzBuzzItem>> PostFizzBuzzItem(FizzBuzzItem fizzBuzzItem)
        {
            CheckFizzBuzz(fizzBuzzItem.Number, ref fizzBuzzItem);

            _context.FizzBuzzItems.Add(fizzBuzzItem);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"New FizzBuzz item created");

            return CreatedAtAction("GetFizzBuzzItem", new { id = fizzBuzzItem.Id }, fizzBuzzItem);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutFizzBuzzItem(long id, FizzBuzzItem fizzBuzzItem)
        {
            if (id != fizzBuzzItem.Id)
            {
                _logger.LogWarning($"[PUT] Couldn't find FizzBuzz item at {id}");
                return BadRequest();
            }

            CheckFizzBuzz(fizzBuzzItem.Number, ref fizzBuzzItem);
            _context.Entry(fizzBuzzItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            _logger.LogInformation($"FizzBuzz item {id} have been updated");

            return NoContent();
        }

        [HttpDelete]
        public async Task<ActionResult<FizzBuzzItem>> DeleteFizzBuzzItems()
        {
            foreach (var fizzBuzzItem in _context.FizzBuzzItems)
            {
                _context.FizzBuzzItems.Remove(fizzBuzzItem);
            }

            await _context.SaveChangesAsync();

            _logger.LogInformation($"FizzBuzz list cleared");

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<FizzBuzzItem>> DeleteFizzBuzzItem(long id)
        {
            var fizzBuzzItem = await _context.FizzBuzzItems.FindAsync(id);

            if (fizzBuzzItem == null)
            {
                _logger.LogWarning($"[DELETE] Couldn't find FizzBuzz item at {id}");
                return NotFound();
            }

            _context.FizzBuzzItems.Remove(fizzBuzzItem);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"FizzBuzz item {id} deleted");

            return NoContent();
        }

        public void CheckFizzBuzz(int value, ref FizzBuzzItem item)
        {
            if (value < 1 || value > 1000)
            {
                _logger.LogWarning($"Invalid number in FizzBuzz item {item.Id}");
                item.Output = $"Invalid Number.";
                return;
            }

            switch (value % 2)
            {
                case 0 when value % 3 == 0:
                    item.Output = $"FizzBuzz";
                    break;
                default:
                {
                    if (value % 3 == 0)
                    {
                        item.Output = $"Buzz";
                    }
                    else if (value % 2 == 0)
                    {
                        item.Output = $"Fizz";
                    }
                    else
                    {
                        item.Output = $"Invalid Number.";
                    }

                    break;
                }
            }

            _logger.LogInformation($"Correct number in FizzBuzz item {item.Id}");
        }
    }
}
