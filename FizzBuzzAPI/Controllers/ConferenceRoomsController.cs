﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FizzBuzzAndRooms.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace FizzBuzzAndRooms.Controllers
{
    [Route("api/rooms")]
    [ApiController]
    public class ConferenceRoomsController : Controller
    {
        private readonly ConferenceRoomContext _context;
        private readonly ILogger _logger;

        public ConferenceRoomsController(ConferenceRoomContext context, ILogger<ConferenceRoomsController> logger)
        {
            _context = context;
            _logger = logger;

            if (_context.ConferenceRooms.Count() != 0) return;

            _context.ConferenceRooms.Add(new ConferenceRoomItem() {Name = "Room 0", Chairs = 20, ProjectorIncluded = true, Tables = 3});
            _context.SaveChanges();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ConferenceRoomItem>>> GetConferenceRoomItems()
        {
            _logger.LogInformation("Getting list of rooms");
            return await _context.ConferenceRooms.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ConferenceRoomItem>> GetConferenceRoomItem(long id)
        {
            var roomItem = await _context.ConferenceRooms.FindAsync(id);

            if (roomItem == null)
            {
                _logger.LogWarning("[GET] Couldn't find room at {id}");
                return NotFound();
            }

            _logger.LogInformation("Getting room {id}");
            return roomItem;
        }

        [HttpPost]
        public async Task<ActionResult<ConferenceRoomItem>> PostConferenceRoomItem(ConferenceRoomItem roomItem)
        {
            _context.ConferenceRooms.Add(roomItem);
            await _context.SaveChangesAsync();

            _logger.LogInformation("Room created");

            return CreatedAtAction("GetConferenceRoomItem", new { id = roomItem.Id }, roomItem);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutConferenceRoomItem(long id, ConferenceRoomItem roomItem)
        {
            if (id != roomItem.Id)
            {
                _logger.LogWarning($"[PUT] Couldn't find room at {id}");
                return BadRequest();
            }

            _context.Entry(roomItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Room {id} have been updated");

            return NoContent();
        }

        [HttpDelete]
        public async Task<ActionResult<ConferenceRoomItem>> DeleteConferenceRoomItems()
        {
            foreach (var roomItem in _context.ConferenceRooms)
            {
                _context.ConferenceRooms.Remove(roomItem);
            }

            await _context.SaveChangesAsync();

            _logger.LogInformation("Room list cleared");

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ConferenceRoomItem>> DeleteConferenceRoomItem(long id)
        {
            var roomItem = await _context.ConferenceRooms.FindAsync(id);

            if (roomItem == null)
            {
                _logger.LogWarning($"[DELETE] Couldn't find room at {id}");
                return NotFound();
            }

            _context.ConferenceRooms.Remove(roomItem);
            await _context.SaveChangesAsync();

            _logger.LogInformation($"Deleted room {id}");

            return NoContent();
        }
    }
}
