﻿namespace FizzBuzzAndRooms.Models
{
    public class FizzBuzzItem
    {
        public long Id { get; set; }
        public int Number { get; set; }
        public string Output { get; set; }
    }
}
