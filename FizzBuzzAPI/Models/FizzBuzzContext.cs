﻿using Microsoft.EntityFrameworkCore;

namespace FizzBuzzAndRooms.Models
{
    public class FizzBuzzContext : DbContext
    {
        public FizzBuzzContext(DbContextOptions<FizzBuzzContext> options)
            : base(options)
        {
        }

        public DbSet<FizzBuzzItem> FizzBuzzItems { get; set; }
    }
}
