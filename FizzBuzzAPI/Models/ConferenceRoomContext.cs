﻿using Microsoft.EntityFrameworkCore;

namespace FizzBuzzAndRooms.Models
{
    public class ConferenceRoomContext : DbContext
    {
        public ConferenceRoomContext(DbContextOptions<ConferenceRoomContext> options) : base(options)
        {

        }

        public DbSet<ConferenceRoomItem> ConferenceRooms { get; set; }
    }
}
