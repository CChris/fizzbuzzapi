﻿namespace FizzBuzzAndRooms.Models
{
    public class ConferenceRoomItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Chairs { get; set; }
        public int Tables { get; set; }
        public bool ProjectorIncluded { get; set; }
    }
}
